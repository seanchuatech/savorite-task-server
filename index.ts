import express, { Request, Response } from 'express';
import mysql2, { Pool, PoolConnection } from 'mysql2';
import cors from 'cors';
import dotenv from 'dotenv';

dotenv.config(); // Load environment variables from .env file

const app = express();
const port = process.env.PORT || 3000;
app.use(cors());
app.use(express.json());

// Create a MySQL connection pool (with type)
const pool: Pool = mysql2.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

// API END POINTS

// GET /tasks: Fetch all tasks
app.get('/tasks', (req: Request, res: Response) => {
  pool.query('SELECT * FROM tasks', (err: mysql2.QueryError | null, results: any) => {
    if (err) throw err;
    res.json(results);
  });
});

// POST /tasks: Create a new task
app.post('/tasks', (req: Request, res: Response) => {
  const { title } = req.body;

  if (!title) {
    return res.status(400).json({ error: 'Title is required' });
  }

  const insertQuery = 'INSERT INTO tasks (title) VALUES (?)';
  pool.query(insertQuery, [title], (err: mysql2.QueryError | null, result: any) => {
    if (err) {
      console.error('Error creating task:', err);
      return res.status(500).json({ error: 'Error creating task' });
    }
    res.status(201).json({ message: 'Task created successfully', taskId: result.insertId });
  });
});

// PUT /tasks/:id: Update a task
app.put('/tasks/:id', (req: Request, res: Response) => {
  const taskId = parseInt(req.params.id);
  const { status } = req.body;

  if (!taskId || !status || (status !== 'pending' && status !== 'completed')) {
    return res.status(400).json({ error: 'Invalid task ID or status' });
  }

  const updateQuery = 'UPDATE tasks SET status = ? WHERE id = ?';
  pool.query(updateQuery, [status, taskId], (err: mysql2.QueryError | null) => {
    if (err) {
      console.error('Error updating task:', err);
      return res.status(500).json({ error: 'Error updating task' });
    }
    res.json({ message: 'Task updated successfully' });
  });
});

// DELETE /tasks/:id: Delete a task
app.delete('/tasks/:id', (req: Request, res: Response) => {
  const taskId = parseInt(req.params.id);

  if (!taskId) {
    return res.status(400).json({ error: 'Invalid task ID' });
  }

  const deleteQuery = 'DELETE FROM tasks WHERE id = ?';
  pool.query(deleteQuery, [taskId], (err: mysql2.QueryError | null) => {
    if (err) {
      console.error('Error deleting task:', err);
      return res.status(500).json({ error: 'Error deleting task' });
    }
    res.json({ message: 'Task deleted successfully' });
  });
});


// Test database connection before starting the server
pool.getConnection((err: any, connection: PoolConnection) => {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    process.exit(1); // Exit the process if the connection fails
  } else {
    console.log('Connected to MySQL database!');

    connection.release(); // Release the connection back to the pool

    // Start the server after successful connection
    app.listen(port, () => {
      console.log(`Server listening at http://localhost:${port}`);
    });
  }
});
