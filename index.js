"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mysql2_1 = __importDefault(require("mysql2"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config(); // Load environment variables from .env file
const app = (0, express_1.default)();
const port = process.env.PORT || 3000;
app.use((0, cors_1.default)());
app.use(express_1.default.json());
// Create a MySQL connection pool (with type)
const pool = mysql2_1.default.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
});
// ... (API endpoints)
// GET /tasks: Fetch all tasks
app.get('/tasks', (req, res) => {
    pool.query('SELECT * FROM tasks', (err, results) => {
        if (err)
            throw err;
        res.json(results);
    });
});
// POST /tasks: Create a new task
app.post('/tasks', (req, res) => {
    const { title } = req.body;
    // Input validation (You can add more validation as needed)
    if (!title) {
        return res.status(400).json({ error: 'Title is required' });
    }
    const insertQuery = 'INSERT INTO tasks (title) VALUES (?)';
    pool.query(insertQuery, [title], (err, result) => {
        if (err) {
            console.error('Error creating task:', err);
            return res.status(500).json({ error: 'Error creating task' });
        }
        res.status(201).json({ message: 'Task created successfully', taskId: result.insertId });
    });
});
// PUT /tasks/:id: Update a task
app.put('/tasks/:id', (req, res) => {
    const taskId = parseInt(req.params.id);
    const { status } = req.body;
    // Input validation
    if (!taskId || !status || (status !== 'pending' && status !== 'completed')) {
        return res.status(400).json({ error: 'Invalid task ID or status' });
    }
    const updateQuery = 'UPDATE tasks SET status = ? WHERE id = ?';
    pool.query(updateQuery, [status, taskId], (err) => {
        if (err) {
            console.error('Error updating task:', err);
            return res.status(500).json({ error: 'Error updating task' });
        }
        res.json({ message: 'Task updated successfully' });
    });
});
// DELETE /tasks/:id: Delete a task
app.delete('/tasks/:id', (req, res) => {
    const taskId = parseInt(req.params.id);
    // Input validation
    if (!taskId) {
        return res.status(400).json({ error: 'Invalid task ID' });
    }
    const deleteQuery = 'DELETE FROM tasks WHERE id = ?';
    pool.query(deleteQuery, [taskId], (err) => {
        if (err) {
            console.error('Error deleting task:', err);
            return res.status(500).json({ error: 'Error deleting task' });
        }
        res.json({ message: 'Task deleted successfully' });
    });
});
// Test database connection before starting the server
pool.getConnection((err, connection) => {
    if (err) {
        console.error('Error connecting to MySQL:', err);
        process.exit(1); // Exit the process if the connection fails
    }
    else {
        console.log('Connected to MySQL database!');
        connection.release(); // Release the connection back to the pool
        // Start the server after successful connection
        app.listen(port, () => {
            console.log(`Server listening at http://localhost:${port}`);
        });
    }
});
